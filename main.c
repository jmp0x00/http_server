#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/time.h>

#define TIMEOUT_SEC		15
#define	TIMEOUT_USEC	0
#define	TYPE_SRC_ADDR	AF_INET
#define	SRC_PORT		8080
#define SRC_ADDR		"127.0.0.1"
#define MAX_CLIENTS		5

int main(int argc, char *argv[])
{
	int server_fd;
	int clients_fd[MAX_CLIENTS];
	int num_client = 0;
	struct sockaddr_in server_addr;
	fd_set rfds;
	struct timeval timeout;
	char buf[1024];
	int bytes_read;
	int i, ret;

	//open internet socket
	server_fd = socket(TYPE_SRC_ADDR, SOCK_STREAM, 0);
	if (server_fd < 0)
	{
		perror("socket");
		exit(1);
	}

	//set non blcok for socket
	if (fcntl(server_fd, F_SETFL, O_NONBLOCK) < 0)
	{
		perror("fcntl");
		exit(2);
	}

	//conver string ip-address to bin
	if (inet_pton(TYPE_SRC_ADDR, SRC_ADDR, &(server_addr.sin_addr)) <= 0)
	{
		perror("inet_pton");
		exit(3);
	}

	//initialize structure address and bind
	server_addr.sin_family = TYPE_SRC_ADDR;
	server_addr.sin_port = htons(SRC_PORT);
	if (bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		perror("bind");
		exit(3);
	}

	if (listen(server_fd, MAX_CLIENTS) < 0)
	{
		perror("listen");
		exit(4);
	}

	FD_ZERO(&rfds);
	FD_SET(server_fd, &rfds);
	for (i = 0; i <= MAX_CLIENTS; i++)
	{
		clients_fd[i] = 0;
	}

	while (1)
	{

		timeout.tv_sec = TIMEOUT_SEC;
		timeout.tv_usec = TIMEOUT_USEC;

		ret = select(MAX_CLIENTS, &rfds, NULL, NULL, &timeout);
		if (ret < 0)
		{
			perror("select");
			exit(5);
		}
		else
		if (ret == 0)
		{
			printf("Timeout\r\n");
			continue;
		}

		if (FD_ISSET(server_fd, &rfds))
		{
			ret = accept(server_fd, NULL, NULL);
			if (ret < 0)
			{
				perror("accept");
				exit(6);
			}
			fcntl(ret, F_SETFL, O_NONBLOCK);
			FD_SET(ret, &rfds);
			if (num_client >= MAX_CLIENTS)
			{
				printf("Warning: maximum clients!\r\n");
			}
			else
			{
				clients_fd[num_client] = ret;
				printf("Added new client(%d)\r\n", num_client);
				num_client++;
			}
		}
		for (i = 0; i < num_client; i++)
		{
			if (FD_ISSET(clients_fd[i], &rfds))
			{
				bytes_read = recv(clients_fd[i], buf, 1024, 0);
				if (bytes_read <= 0)
				{
					close(clients_fd[i]);
					clients_fd[i] = 0;//FIXME
					continue;
				}
				ret = send(clients_fd[i], buf, bytes_read, 0);
				if (ret < 0)
				{
					close(clients_fd[i]);
					clients_fd[i] = 0;
					continue;
				}
			}
		}
	}
	return 0;
}

